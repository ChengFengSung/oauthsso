﻿using Microsoft.EntityFrameworkCore;

namespace ChengFengOAuth.Entities
{
    public class SubscriberContext : DbContext
    {
        public SubscriberContext(DbContextOptions<SubscriberContext> options) : base(options)
        {
        }

        public DbSet<Subscriber> Subscriber { get; set; }
    }
}
