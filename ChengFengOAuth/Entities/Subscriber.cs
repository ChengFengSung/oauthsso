﻿namespace ChengFengOAuth.Entities
{
    public class Subscriber
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
        public string AccessToken { get; set; }
    }
}
