﻿namespace ChengFengOAuth.Models.Line
{
    public class LineVerifyEndpointResponse
    {
        private string iss;
        private string sub;
        private string aud;
        private int exp;
        private int iat;
        private string nonce;
        private List<string> amr;
        private string name;
        private string picture;
        private string? email;

        public string Iss { get => iss; set => iss = value; }
        public string Sub { get => sub; set => sub = value; }
        public string Aud { get => aud; set => aud = value; }
        public int Exp { get => exp; set => exp = value; }
        public int Iat { get => iat; set => iat = value; }
        public string Nonce { get => nonce; set => nonce = value; }
        public List<string> Amr { get => amr; set => amr = value; }
        public string Name { get => name; set => name = value; }
        public string Picture { get => picture; set => picture = value; }
        public string? Email { get => email; set => email = value; }
    }
}
