﻿namespace ChengFengOAuth.Models.Line
{
    public class LineAccessTokenResponse
    {
        private string access_token;
        private int expires_in;
        private string id_token;
        private string refresh_token;
        private string scope;
        private string token_type;

        public string Access_token { get => access_token; set => access_token = value; }
        public int Expires_in { get => expires_in; set => expires_in = value; }
        public string Id_token { get => id_token; set => id_token = value; }
        public string Refresh_token { get => refresh_token; set => refresh_token = value; }
        public string Scope { get => scope; set => scope = value; }
        public string Token_type { get => token_type; set => token_type = value; }
    }
}
