﻿namespace ChengFengOAuth.Models.LineNotify
{
    public class LineNotifyAccessTokenResponse
    {
        private string access_token;

        public string Access_Token { get => access_token; set => access_token = value; }
    }
}
