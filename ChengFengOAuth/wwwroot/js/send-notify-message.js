﻿$("#messageSubmit").click(function () {
    var message = { message: $("#message").val() };
    $.ajax({
        url: "https://line-login-starter-20220327.herokuapp.com/Notification/SendMessageToSubscriber",
        method: "post",
        data: message,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function (res) {
            toastr.options.positionClass = "toast-bottom-right";
            toastr.options.extendedTimeOut = 0;
            toastr.options.timeOut = 1000;
            toastr.options.fadeOut = 250;
            toastr.options.fadeIn = 250;
            toastr['success']("訊息已傳送");
        },
        error: function (res) {
            toastr.options.positionClass = "toast-bottom-right";
            toastr.options.extendedTimeOut = 0;
            toastr.options.timeOut = 1000;
            toastr.options.fadeOut = 250;
            toastr.options.fadeIn = 250;
            toastr['error']("訊息傳送失敗");
        }
    })
});