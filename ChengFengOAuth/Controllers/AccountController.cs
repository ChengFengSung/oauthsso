﻿using ChengFengOAuth.Models;
using ChengFengOAuth.Models.Line;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Security.Claims;
using System.Web;

namespace ChengFengOAuth.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private const string _lineAuthURL = "https://access.line.me/oauth2/v2.1/authorize";
        private const string _lineAccessTokenURL = "https://api.line.me/oauth2/v2.1/token";
        private const string _lineInfoEndpoint = "https://api.line.me/oauth2/v2.1/verify";
        private readonly string _lineClientId, _lineClientSecret;

#pragma warning disable CS8618 // 退出建構函式時，不可為 Null 的欄位必須包含非 Null 值。請考慮宣告為可為 Null。
        private static IMemoryCache _memoryCache;
#pragma warning restore CS8618 // 退出建構函式時，不可為 Null 的欄位必須包含非 Null 值。請考慮宣告為可為 Null。
        private readonly HttpClient _httpClient;
        private string GetRedirectUrl() => $"https://{HttpContext.Request.Host}/Account/CallBack";

        public AccountController(IMemoryCache memoryCache, IHttpClientFactory httpClientFactory)
        {
            _memoryCache = memoryCache;
            _httpClient = httpClientFactory.CreateClient();
            _lineClientId = Environment.GetEnvironmentVariable("Line_Client_ID") ?? throw new ArgumentNullException(nameof(_lineClientId));
            _lineClientSecret = Environment.GetEnvironmentVariable("Line_Client_Secret") ?? throw new ArgumentNullException(nameof(_lineClientSecret));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Signon()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult LineSignon()
        {
            var uriBuilder = new UriBuilder(_lineAuthURL);

            string state = Guid.NewGuid().ToString();
            SaveStateToCache(state);

            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["response_type"] = "code";
            query["client_id"] = _lineClientId;
            query["redirect_uri"] = GetRedirectUrl();
            query["state"] = state;
            query["scope"] = "profile openid email";
            uriBuilder.Query = query.ToString();

            string url = uriBuilder.ToString();
            return Redirect(url);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> CallBack(string? code, string? state, bool friendship_status_changed, string? error, string? error_description)
        {
            if (_memoryCache.TryGetValue(state, out string value) == false)
                return BadRequest();
            else 
                _memoryCache.Remove(state);

            if (code == null)
                throw new ArgumentNullException(nameof(code));

            LineAccessTokenResponse? accessTokenResponse = await RequestLineTokenAsync(code);

            if (accessTokenResponse == null) throw new ArgumentNullException(nameof(accessTokenResponse));

            LineVerifyEndpointResponse? endpointResponse = await GetUserInfoThrowAccessTokenAsync(accessTokenResponse.Id_token);

            if (endpointResponse == null) return ReturnErrorHtml();

            SignInForUser(endpointResponse);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> Signout()
        {
            await HttpContext.SignOutAsync();

            return RedirectToAction("Signon", "Account");
        }

        private void SaveStateToCache(string state)
        {
            _memoryCache.Set(state, state, DateTimeOffset.Now.AddMinutes(3));
        }

        private void SignInForUser(LineVerifyEndpointResponse endpointResponse)
        {
            var lineClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, endpointResponse.Name),
                new Claim("PictureURL", endpointResponse.Picture),
                new Claim("UserId", endpointResponse.Sub),
            };
            if (endpointResponse.Email != null)
                lineClaims.Add(new Claim(ClaimTypes.Email, endpointResponse.Email));

            var lineIdentity = new ClaimsIdentity(lineClaims, "Line Ideneity");
            var userPrincipal = new ClaimsPrincipal(new[] { lineIdentity });

            TempData["UserName"] = endpointResponse.Name;

            HttpContext.SignInAsync(userPrincipal);
        }

        private async Task<LineVerifyEndpointResponse?> GetUserInfoThrowAccessTokenAsync(string id_token)
        {
            Dictionary<string, string> body = new Dictionary<string, string>()
            {
                {"id_token", id_token},
                {"client_id", _lineClientId}
            };

            var request = new HttpRequestMessage(HttpMethod.Post, _lineInfoEndpoint)
            {
                Content = new FormUrlEncodedContent(body)
            };

            HttpResponseMessage responseMessage = await _httpClient.SendAsync(request);

            if (responseMessage.IsSuccessStatusCode)
            {
                LineVerifyEndpointResponse? response = JsonConvert.DeserializeObject<LineVerifyEndpointResponse>(
                await responseMessage.Content.ReadAsStringAsync());

                return response;
            }
            return null;
        }

        private async Task<LineAccessTokenResponse?> RequestLineTokenAsync(string code)
        {
            Dictionary<string, string> body = new Dictionary<string, string>()
            {
                {"grant_type", "authorization_code"},
                {"code", code},
                {"redirect_uri", GetRedirectUrl()},
                {"client_id", _lineClientId},
                {"client_secret", _lineClientSecret}
            };

            var request = new HttpRequestMessage(HttpMethod.Post, _lineAccessTokenURL)
            {
                Content = new FormUrlEncodedContent(body)
            };

            HttpResponseMessage responseMessage = await _httpClient.SendAsync(request);

            if (responseMessage.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<LineAccessTokenResponse>(await responseMessage.Content.ReadAsStringAsync());
            else
                throw new Exception("Line login response failure status code.");
        }

        private ViewResult ReturnErrorHtml()
        {
            return View("Views/Shared/Error.cshtml", new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
