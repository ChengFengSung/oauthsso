﻿using ChengFengOAuth.Entities;
using ChengFengOAuth.Models.LineNotify;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Web;

namespace ChengFengOAuth.Controllers
{
    [Authorize]
    public class SubscriptionController : Controller
    {
        private const string _lineNotifyAuthURL = "https://notify-bot.line.me/oauth/authorize";
        private const string _lineNotifyTokenURL = "https://notify-bot.line.me/oauth/token";
        private const string _lineNotifyRevokeURL = "https://notify-api.line.me/api/revoke";
        private readonly string _lineNotifyClientId, _lineNotifyClientSecret;
        private readonly IMemoryCache _memoryCache;
        private readonly HttpClient _httpClient;
        private readonly SubscriberContext _subscriberContext;

        private string GetRedirectUrl() => $"https://{HttpContext.Request.Host}/Subscription/CallBack";

        public SubscriptionController(IMemoryCache memoryCache, IHttpClientFactory httpClientFactory, SubscriberContext subscriberContext)
        {
            _memoryCache = memoryCache;
            _httpClient = httpClientFactory.CreateClient();
            _subscriberContext = subscriberContext;
            _lineNotifyClientId = Environment.GetEnvironmentVariable("Line_Notify_Client_ID") ?? throw new ArgumentNullException(nameof(_lineNotifyClientId));
            _lineNotifyClientSecret = Environment.GetEnvironmentVariable("Line_Notify_Client_Secret") ?? throw new ArgumentNullException(nameof(_lineNotifyClientSecret));
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Subscribe()
        {
            var urlBuilder = new UriBuilder(_lineNotifyAuthURL);

            string state = Guid.NewGuid().ToString();
            SaveStateToCache(state);

            var query = HttpUtility.ParseQueryString(urlBuilder.Query);
            query["response_type"] = "code";
            query["client_id"] = _lineNotifyClientId;
            query["redirect_uri"] = GetRedirectUrl();
            query["scope"] = "notify";
            query["state"] = state;
            urlBuilder.Query = query.ToString();

            string url = urlBuilder.ToString();
            return Redirect(url);
        }

        public async Task<IActionResult> CallBack(string? code, string? state, string? error, string? error_description)
        {
            if (_memoryCache.TryGetValue(state, out string value) == false)
                return BadRequest();
            else
                _memoryCache.Remove(state);

            if (code == null)
                throw new ArgumentNullException(nameof(code));

            LineNotifyAccessTokenResponse? accessTokenResponse = await RequestLineNotifyAsync(code);
            if (accessTokenResponse == null) throw new ArgumentNullException(nameof(accessTokenResponse));

            if (accessTokenResponse.Access_Token == null) throw new ArgumentNullException(nameof(accessTokenResponse.Access_Token));

            await _subscriberContext.Subscriber.AddAsync(new Subscriber()
            {
                Id = Guid.NewGuid(),
                Name = GetNameInClaim() ?? string.Empty,
                Email = GetEmailInClaim() ?? string.Empty,
                UserId = GetUserIdClaim() ?? string.Empty,
                AccessToken = accessTokenResponse.Access_Token,
            });
            _subscriberContext.SaveChanges();

            TempData["Subscribe"] = true;
            return RedirectToAction("SubscribeSuccessfully", "Subscription");
        }

        public IActionResult SubscribeSuccessfully()
        {
            List<Subscriber> subscribers = (from subscriber in _subscriberContext.Subscriber select subscriber).ToList();
            return View(subscribers);
        }

        public IActionResult Unsubscribe()
        {
            string userName;
            var result = TempData.First(item => item.Key == "UserName");
            if (result.Value == null) 
                throw new KeyNotFoundException("UserName");
            else  
                userName = result.Value.ToString() ?? throw new KeyNotFoundException("UserName");
            return View();
        }

        public IActionResult UnsubscribeAction()
        {
            string userName = GetNameInClaim() ?? throw new KeyNotFoundException("Name in claim");
            string userId = GetUserIdClaim() ?? throw new KeyNotFoundException("User ID in claim");

            Subscriber? subscriber = (from sub in _subscriberContext.Subscriber
                                                where sub.Name == userName && sub.UserId == userId
                                                select sub).FirstOrDefault();

            if (subscriber == null) throw new KeyNotFoundException("SubscriberContext can't found subscriber.");

            RevokeLineAccessToken(subscriber.AccessToken);

            _subscriberContext.Subscriber.Remove(subscriber);
            _subscriberContext.SaveChanges();

            TempData["Subscribe"] = false;
            return RedirectToAction("UnsubscribeSuccessfully", "Subscription");
        }

        public IActionResult UnsubscribeSuccessfully()
        {
            return View();
        }

        private async void RevokeLineAccessToken(string access_token)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, _lineNotifyRevokeURL);
            request.Content = new FormUrlEncodedContent(new Dictionary<string, string>());
            request.Headers.Add("Authorization", $"Bearer {access_token}");

            HttpResponseMessage response = await _httpClient.SendAsync(request);

            if (!response.IsSuccessStatusCode)
                throw new Exception("Fail to revoke access token.");
        }

        private string? GetNameInClaim()
        {
            return (from claim in HttpContext.User.Claims where claim.Type == ClaimTypes.Name select claim.Value).SingleOrDefault();
        }

        private string? GetEmailInClaim()
        {
            return (from claim in HttpContext.User.Claims where claim.Type == ClaimTypes.Email select claim.Value).SingleOrDefault();
        }

        private string? GetUserIdClaim()
        {
            return (from claim in HttpContext.User.Claims where claim.Type == "UserId" select claim.Value).SingleOrDefault();
        }

        private void SaveStateToCache(string state)
        {
            _memoryCache.Set(state, state, DateTimeOffset.Now.AddMinutes(3));
        }

        private async Task<LineNotifyAccessTokenResponse?> RequestLineNotifyAsync(string code)
        {
            Dictionary<string, string> body = new Dictionary<string, string>()
            {
                {"grant_type", "authorization_code"},
                {"code", code},
                {"redirect_uri", GetRedirectUrl()},
                {"client_id", _lineNotifyClientId},
                {"client_secret", _lineNotifyClientSecret},
            };

            var request = new HttpRequestMessage(HttpMethod.Post, _lineNotifyTokenURL)
            {
                Content = new FormUrlEncodedContent(body)
            };

            HttpResponseMessage responseMessage = await _httpClient.SendAsync(request);

            if (responseMessage.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<LineNotifyAccessTokenResponse>(await responseMessage.Content.ReadAsStringAsync());

            throw new Exception("Line Notify response failure status code.");
        }
    }
}
