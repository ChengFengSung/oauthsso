﻿using ChengFengOAuth.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ChengFengOAuth.Controllers
{
    [Authorize]
    public class NotificationController : Controller
    {
        private const string _lineNotifyEndpoint = "https://notify-api.line.me/api/notify";
        private readonly SubscriberContext _subscriberContext;
        private readonly HttpClient _httpClient;

        public NotificationController(SubscriberContext subscriberContext, IHttpClientFactory httpClientFactory)
        {
            _subscriberContext = subscriberContext;
            _httpClient = httpClientFactory.CreateClient();
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SendMessageToSubscriber( string? message)
        {
            if (string.IsNullOrEmpty(message))
                return BadRequest();
            List<Subscriber> subscribers = (from sub in _subscriberContext.Subscriber select sub).ToList();

            foreach(Subscriber subscriber in subscribers)
            {
                await SendMessage(subscriber.AccessToken, message);
            }

            await Task.CompletedTask;
            return new JsonResult(new { success = true});
        }

        private async Task<bool> SendMessage(string access_token, string message)
        {
            Dictionary<string, string> body = new Dictionary<string, string>()
            {
                {"message", message},
                {"stickerPackageId", "11537"},
                {"stickerId", "52002752"},
            };

            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, _lineNotifyEndpoint);
            requestMessage.Content = new FormUrlEncodedContent(body);
            requestMessage.Headers.Add("Authorization", $"Bearer {access_token}");
            HttpResponseMessage responseMessage = await _httpClient.SendAsync(requestMessage);

            return responseMessage.IsSuccessStatusCode;
        }
    }
}
