﻿using ChengFengOAuth.Entities;
using ChengFengOAuth.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;

namespace ChengFengOAuth.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly SubscriberContext _subscriberContext;

        public HomeController(SubscriberContext subscriberContext)
        {
            _subscriberContext = subscriberContext;
        }

        public IActionResult Index()
        {
            Claim? pictureUrlClaim = User.Claims.FirstOrDefault(claim => claim.Type == "PictureURL");
            string? pictureUrl = pictureUrlClaim?.Value;
            ViewData["PictureUrl"] = pictureUrl;

            Claim? nameClaim = User.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Name);
            string? userName = nameClaim?.Value;
            ViewData["UserName"] = userName;

            Claim? emailClaim = User.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Email);
            string? email = emailClaim?.Value;
            ViewData["UserEmail"] = email;

            ViewData["Subscribe"] = (userName == null || email == null) ? null :
                (from sub in _subscriberContext.Subscriber where sub.Name == userName && sub.Email == email select sub.AccessToken).Any();

            List<Subscriber> subscribers = (from subscriber in _subscriberContext.Subscriber select subscriber).ToList();
            ViewData["SubscriberCount"] = subscribers.Count;

            return View();
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}